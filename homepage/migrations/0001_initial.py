# Generated by Django 2.2.5 on 2019-10-08 04:00

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Jadwal',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama_kegiatan', models.CharField(max_length=50)),
                ('kategori', models.CharField(max_length=50)),
                ('tempat', models.CharField(max_length=50)),
                ('waktu', models.CharField(max_length=50)),
            ],
        ),
    ]
