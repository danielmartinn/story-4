from django.db import models
from django.utils import timezone
import datetime
# Create your models here.

class Jadwal(models.Model):
	nama_kegiatan = models.CharField(max_length = 50)
	kategori = models.CharField(max_length = 50)
	tempat = models.CharField(max_length = 50)
	tanggal = models.DateField(default = timezone.now)
	waktu = models.TimeField(default = timezone.now)

	def __str__(self):
		return self.nama_kegiatan