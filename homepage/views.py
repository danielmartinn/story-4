from django.shortcuts import render, redirect 
from django.http import HttpResponse
from .forms import FormJadwal
from .models import Jadwal
from django.utils import timezone
import datetime


# Create your views here.
def index(request):
    return render(request, 'index.html')

def gallery(request):
    return render(request, 'gallery.html')

def createJadwal(request):
	form =	FormJadwal()
	if request.method == "POST":
		form = FormJadwal(request.POST)
		if form.is_valid():
			form.save()
		return redirect('homepage:jadwal')
	return render(request, 'createJadwal.html', {'form' : form})

def jadwal(request):
	schedule = Jadwal.objects.order_by("tanggal", "waktu")
	response = {
		'jadwal' : schedule,
		'server_time' : timezone.now(),
	}
	return render(request, "jadwal.html", response)

def delete(request):
	if request.method == "POST":
		id = request.POST['id']
		Jadwal.objects.get(id=id).delete()
	return redirect('homepage:jadwal')



