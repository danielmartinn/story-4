from django.urls import path
from django.contrib import admin
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name = 'index'),
    path('gallery/', views.gallery, name = 'gallery'),
    path('jadwal/', views.jadwal, name = 'jadwal'),
    path('createjadwal/', views.createJadwal, name = 'createJadwal'),
    path('jadwal/delete', views.delete, name = "delete"),

    # dilanjutkan ...
]