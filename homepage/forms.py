from . import models
from django import forms

class FormJadwal(forms.ModelForm):
    nama_kegiatan = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "jadwalfields full",
        "required" : True,
        "placeholder": "ex : Deadline PR 2 Matdas",
        }))

    kategori = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "jadwalfields full",
        "required" : True,
        "placeholder":"ex : Tugas",

        }))

    tempat = forms.CharField(widget = forms.TextInput(attrs={
        "class" : "jadwalfields full",
        "required" : True,
        "placeholder":"ex : Lab Babe",
        }))

    tanggal = forms.DateField(widget = forms.SelectDateWidget(attrs={
        "class" : "datefield jadwalfields",
        "required" : True,
        }))

    waktu = forms.TimeField(widget = forms.TextInput(attrs={
        "class" : "datefield jadwalfields",
        "required" : True,
        "placeholder":"ex : 00:00",
        }))

    class Meta:
    	model = models.Jadwal
    	fields = ["nama_kegiatan", "kategori", "tempat", "tanggal", "waktu"]